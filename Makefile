build:
	clear
	g++ addqueue.cpp -o addqueue -lboost_system -lboost_filesystem -I /usr/lib/
	g++ showqueue.cpp -o showqueue -lboost_system -lboost_filesystem -I /usr/lib/
	g++ rmqueue.cpp -o rmqueue -lboost_system -lboost_filesystem -I /usr/lib/
	chmod 4751 addqueue
	chmod 4751 showqueue
	chmod 4751 rmqueue

install:
	sudo useradd print
	sudo apt-get install build-essential
	sudo apt-get install libboost-all-dev
	sudo apt-get install libbz2-dev
	mkdir /home/spool
	sudo chown -R print:root /home/spool
	chmod 770 /home/spool

test:
	./addqueue addqueue.cpp addqueue.cpp showqueue.cpp Makefile
	echo -e "\n"
	./showqueue
	echo -e "\n"
	./rmqueue addqueue.cpp1
	echo -e "\n"
	./showqueue
	./addqueue *.*
	./addqueue *.*
	./addqueue *.*
	echo -e "\n"
	./showqueue
	echo -e "Begin cleanup\n"
	./rmqueue ~.~
	
clean:
	-rm *.gch &> /dev/null
	-rm *.out &> /dev/null
	echo "Everything Cleaned"