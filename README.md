# Security Architecture and Engineering
## Assignment 2
[https://www.cs.columbia.edu/~smb/classes/f15/pgm2.txt](https://www.cs.columbia.edu/~smb/classes/f15/pgm2.txt)

COMS W4187 - Assignment 2 - *Privileged Programs* - Implement (something like) a print spooler
* Due Wednesday, November 11

### Folder Structure
```
.
├── Assn\ 2\ details
├── Makefile
├── addqueue.cpp
├── folder.hpp
├── rmqueue.cpp
├── showqueue.cpp
└── vminfo.txt
```

### Dependencies
- [Boost C++ Libraries](http://www.boost.org)

### Installation Instructions
Use the makefile
* `make install`
* `make build`

### Testing
Run `make test`
