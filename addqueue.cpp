#include <iostream>
#include <fstream>
#include <ctime>
#include <string>
#include <libgen.h> 
#include <stdlib.h> 
#include <boost/filesystem.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/functional/hash.hpp>
#include "folder.hpp"

using namespace std;
using namespace boost::filesystem;

int addFile(char* filepath)
{
    path p (filepath);
    boost::hash<std::string> string_hash;
    // cout<<"uid : "<<getuid()<<endl;
    string userid = boost::lexical_cast<string>(getuid());
    string final_name = userid+':'+p.filename().string();
    string dest_path = HOME_DIR + final_name;
    // cout<<dest_path<<endl;
    path dest(dest_path);
    int file_counter = 1;
    while (exists(dest))
    {
        string counter_str = boost::lexical_cast<string>(file_counter);
        path dest2(dest_path+counter_str);
        dest = dest2;
        file_counter++;
    }
    try
    {
        copy_file(filepath, dest );
        cout <<p.filename().string()<<": "<<"Y "<<string_hash(dest.filename().string()) <<endl;
    }
    catch (const filesystem_error& ex)
    {
        cout <<final_name<<": "<<"X "<<ex.what() <<endl;
        // cout <<final_name<<": " << '\n';
    }

    return 0;
}

int main(int argc, char* argv[]) {

    // int i = 0;
    for (int i=1; i< argc; i++)
    {
        addFile(argv[i]);    
    }
}