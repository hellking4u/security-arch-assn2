#include <iostream>
#include <fstream>
#include <ctime>
#include <string>
#include <libgen.h> 
#include <stdlib.h> 
#include <boost/filesystem.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/functional/hash.hpp>
#include <boost/algorithm/string.hpp>
#include "folder.hpp"

using namespace std;
using namespace boost::filesystem;
using namespace boost;

int deleteFile(char* filepath)
{
    string fpath(filepath);
    boost::hash<std::string> string_hash;
    string userid = boost::lexical_cast<string>(getuid());
    try
    {
        path targetDir(HOME_DIR); 

        directory_iterator it(targetDir), eod;
        for( directory_iterator it(targetDir) ; it != eod ; ++it )
        { 
            path p = *it;
            if(is_regular_file(p))
            {
                string fname = p.filename().string();
                // cout<<"Looking at "<<fname<<" Target :"<<fpath<<endl;
                typedef vector< string > split_vector_type;

                split_vector_type SplitVec; // #2: Search for tokens
                split( SplitVec, fname, is_any_of(":"), token_compress_on ); // SplitVec == { "hello abc","ABC","aBc goodbye" }

                string owner_id = SplitVec[0];
                // merge the filename back as well. All this to handle edge case of : inside the filename.
                string orig_fname;
                for(size_t i=1;i!=SplitVec.size();++i)
                {
                    orig_fname += SplitVec[i] + ":";
                }
                if (orig_fname.size () > 0)  orig_fname.resize (orig_fname.size () - 1);

                if (owner_id==userid && orig_fname==fpath)
                {
                    boost::filesystem::remove(p);
                    cout<<orig_fname<<": Y"<<endl;
                }
                if (orig_fname==fpath && owner_id!=userid)
                {
                    cout <<orig_fname<<": X "<<"User not authorized to delete." <<endl;
                    return -1;
                }
                if (fpath == "~.~" && owner_id==userid)
                {
                    boost::filesystem::remove(p);
                    cout<<orig_fname<<": Y"<<endl;
                }
            } 
        }
    }
    catch (const filesystem_error& ex)
    {
        cout <<fpath<<": X "<<ex.what() <<endl;
        // cout <<final_name<<": " << '\n';
    }

    return 0;
}

int main(int argc, char* argv[]) {

    // int i = 0;
    for (int i=1; i< argc; i++)
    {
        deleteFile(argv[i]);    
    }
}