#include <iostream>
#include <iterator>
#include <algorithm>
#include <boost/filesystem.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/foreach.hpp> 
#include <numeric>
#include <map>
#include <ctime>
#include <boost/functional/hash.hpp>

#include "folder.hpp"

using namespace std;
using namespace boost::filesystem;
using namespace boost;
int main(int argc, char* argv[])
{

    path spool (HOME_DIR);   // p reads clearer than argv[1] in the following code
    boost::hash<std::string> string_hash;
    try
    {
        if (exists(spool))    // does p actually exist?
        {
            typedef multimap<time_t, path> result_set_t;
            result_set_t result_set;

            directory_iterator end_itr; // default construction yields past-the-end

            if ( exists(spool) && is_directory(spool))
            {
              for( directory_iterator dir_iter(spool) ; dir_iter != end_itr ; ++dir_iter)
              {
                if (is_regular_file(dir_iter->status()) )
                {
                  result_set.insert(result_set_t::value_type(last_write_time(dir_iter->path()), *dir_iter));
                }
              }
            }

            for (result_set_t::iterator it=result_set.begin();it!=result_set.end(); it++)
            {
                path p = (*it).second;
                time_t date = (*it).first;
                // cout<<p.filename().string()<<endl;
                if(is_regular_file(p))
                {
                    string fname = p.filename().string();
                    // cout<<fname<<endl;
                    typedef vector< string > split_vector_type;

                    split_vector_type SplitVec; // #2: Search for tokens
                    split( SplitVec, fname, is_any_of(":"), token_compress_on ); // SplitVec == { "hello abc","ABC","aBc goodbye" }

                    // merge the filename back as well. All this to handle edge case of : inside the filename.
                    string orig_fname;
                    for(size_t i=1;i!=SplitVec.size();++i)
                    {
                        orig_fname += SplitVec[i] + ":";
                    }
                    if (orig_fname.size () > 0)  orig_fname.resize (orig_fname.size () - 1);

                    //set the time properly as well
                    char buffer[80];
                    struct tm * curtime = localtime(&date);
                    strftime(buffer,80,"%Y-%m-%d %I:%M:%S",curtime);
                    string date_string(buffer);
                    cout << string_hash(orig_fname)<<" ";
                    cout<<SplitVec[0]<<" ";
                    cout<<date_string<<" ";
                    cout<<orig_fname<<endl;
                    // cout<< << " size is " << file_size(p) <<" date "<<ctime(&date)<<endl;
                }
            }
        }
        else
            cout << spool << " does not exist\n";
    }

  catch (const filesystem_error& ex)
  {
    cout << ex.what() << '\n';
  }

  return 0;
}